# How to record onto DVD from VHS (Toshiba DVR20)

## Setup
1. Plug in the HDMI cable from the TV (HDMI 1) to the Toshiba (HDMI slot) 
1. Plug in the Toshiba power and turn on
1. On the TV, press `AV` and select the channel (HDMI 1)
1. On the Toshiba, press `Setup` > `General Settings` > Recording
	1. `Auto Chapter` > `15 Mins`
	1. `Dubbing Mode` > `VCR > DVD`
	1. `Auto Finalise` > `OFF`
	1. `Video Colour System` > `Auto`
1. Press `Back` a few times until you're back to the blank screen

####Notes

##### Buttons
* Press `Return / Back` to exit and menu
* Press `Enter / Ok` To select anything

##### The DVD's we used were
* DVD+R
* 16X speed
* 120Min 
* 4.7 GB


## Recording

### Get the VCR ready
1. Insert Video into Toshiba
1. On the Toshiba, switch to control the VCR, `VCR`
1. Rewind, `Rev` the tape the beginning
1. Eject it, `Open / Close` to check it's at the beginning 
1. Put it back in to reset the timer to 00:00:00
1. Stop and Fast Forward it to the end to see how long the tape is, we need one of the following categories
	* Under, 60mins
	* Under, 120mins
1. Rewind it back to 00:00:00 (the beginning)

### Get the DVD ready
1. On the Toshiba, switch to DVD, `DVD`
1. Open the tray, `Open / Close`, and put in a blank "+R" DVD, close the tray, `Open / Close`
1. Let the Toshiba do its bits (a "Loading" bar shows for a minute or two)

### Recording checklist
* Is the Tape in the right place?
* Is the Tape under 60 mins, or you're ok with splitting it?
* Can you see a picture and hear sound when you watch the VCR?

### Start Recording

1. Press `REC MODE`
	* XP for 60 mins (Best Quality)
	* SP for 120 mins (Ok Quality)
	* LP / EP / SLP (Nope… do not use these qualities)

1. Switch to VCR, `VCR`
1. Start Dubbing `DUBBING`
	* You should see "VCR > DVD" appear on the screen, then the video starts playing with sound.
	* The little screen on the front of the Toshiba, you should see a red "DB" appear
1. Let it run with sound on, don't stop it, pause it, rewind it

### Recording finished?
1. Press stop, let it "write to disk"

### Titles
* It auto creates chapters every 15 mins on the DVD recording
* If you want to edit the DVD title, look in the manual, pg. 57

### Finalise it
* Has it finished recording?
* Got everything on it you want? 

-

1. Switch to DVD, [DVD]
1. Press Setup, `SETUP` > `DVD Menu` > `Finalise`
1. Select [Yes]
1. Let it "finalise"

# Done
* Copy it and send it to everyone! :)


